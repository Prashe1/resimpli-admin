import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditTargetCampaignComponent } from './add-edit-target-campaign.component';

describe('AddEditTargetCampaignComponent', () => {
  let component: AddEditTargetCampaignComponent;
  let fixture: ComponentFixture<AddEditTargetCampaignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditTargetCampaignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditTargetCampaignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
