import { ListComponent } from "./list/list.component";
import { AddEditTargetCampaignComponent } from "./add-edit-target-campaign/add-edit-target-campaign.component";

export const components = [ListComponent, AddEditTargetCampaignComponent];