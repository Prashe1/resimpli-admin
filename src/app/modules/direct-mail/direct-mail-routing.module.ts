import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import * as fromManageTemplates from "./manage-templates";
import * as fromMailingTemplates from "./mailing-templates";
import * as fromEnvelopType from "./envelop-type";
import * as fromPostageType from "./postage-type";
import * as fromManageDiscount from "./manage-discount";
import * as fromManageCoupons from "./manage-coupons";
import * as fromManageTargetCampaign from "./manage-target-campaign";


const routes: Routes = [
  {
    path: "manage-template",
    children: [
      {
        path: "",
        component: fromManageTemplates.components[0],
      },
      {
        path: "create",
        component: fromManageTemplates.components[1],
        data: {
          type: "add",
        }
      },
      {
        path: "edit/:id",
        component: fromManageTemplates.components[1],
        data: {
          type: "edit",
        }
      }
    ]
  },
  {
    path: "mailing-template",
    children: [
      {
        path: "",
        component: fromMailingTemplates.components[0],
      },
      {
        path: "create",
        component: fromMailingTemplates.components[1],
        data: {
          type: "add",
        }
      },
      {
        path: "edit/:id",
        component: fromMailingTemplates.components[1],
        data: {
          type: "edit",
        }
      }
    ]
  },
  {
    path: "envelop-type",
    children: [
      {
        path: "",
        component: fromEnvelopType.components[0],
      },
      {
        path: "create",
        component: fromEnvelopType.components[1],
        data: {
          type: "add",
        }
      },
      {
        path: "edit/:id",
        component: fromEnvelopType.components[1],
        data: {
          type: "edit",
        }
      }
    ]
  },
  {
    path: "postage-type",
    children: [
      {
        path: "",
        component: fromPostageType.components[0],
      },
      {
        path: "create",
        component: fromPostageType.components[1],
        data: {
          type: "add",
        }
      },
      {
        path: "edit/:id",
        component: fromPostageType.components[1],
        data: {
          type: "edit",
        }
      }
    ]
  },
  {
    path: "manage-discount",
    children: [
      {
        path: "",
        component: fromManageDiscount.components[0],
      },
      {
        path: "create",
        component: fromManageDiscount.components[1],
        data: {
          type: "add",
        }
      },
      {
        path: "edit/:id",
        component: fromManageDiscount.components[1],
        data: {
          type: "edit",
        }
      }
    ]
  },
  {
    path: "manage-coupons",
    children: [
      {
        path: "",
        component: fromManageCoupons.components[0],
      },
      {
        path: "create",
        component: fromManageCoupons.components[1],
        data: {
          type: "add",
        }
      },
      {
        path: "edit/:id",
        component: fromManageCoupons.components[1],
        data: {
          type: "edit",
        }
      }
    ]
  },
  {
    path: "target-campaign",
    children: [
      {
        path: "",
        component: fromManageTargetCampaign.components[0],
      },
      {
        path: "create",
        component: fromManageTargetCampaign.components[1],
        data: {
          type: "add",
        }
      },
      {
        path: "edit/:id",
        component: fromManageTargetCampaign.components[1],
        data: {
          type: "edit",
        }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DirectMailRoutingModule { }
