import { ListComponent } from "./list/list.component";
import { AddEditDcouponComponent } from "./add-edit-dcoupon/add-edit-dcoupon.component";

export const components = [ListComponent, AddEditDcouponComponent];