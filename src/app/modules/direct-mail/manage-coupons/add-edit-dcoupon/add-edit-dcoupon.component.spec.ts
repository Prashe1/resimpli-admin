import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditDcouponComponent } from './add-edit-dcoupon.component';

describe('AddEditDcouponComponent', () => {
  let component: AddEditDcouponComponent;
  let fixture: ComponentFixture<AddEditDcouponComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditDcouponComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditDcouponComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
