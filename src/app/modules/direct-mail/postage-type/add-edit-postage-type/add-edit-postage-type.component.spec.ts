import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditPostageTypeComponent } from './add-edit-postage-type.component';

describe('AddEditPostageTypeComponent', () => {
  let component: AddEditPostageTypeComponent;
  let fixture: ComponentFixture<AddEditPostageTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditPostageTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditPostageTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
