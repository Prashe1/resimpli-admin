import { ListComponent } from "./list/list.component";
import { AddEditPostageTypeComponent } from "./add-edit-postage-type/add-edit-postage-type.component";

export const components = [ListComponent, AddEditPostageTypeComponent];