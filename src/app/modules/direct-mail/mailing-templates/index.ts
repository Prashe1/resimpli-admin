import { ListComponent } from "./list/list.component";
import { AddEditMailingTemplateComponent } from "./add-edit-mailing-template/add-edit-mailing-template.component";

export const components = [ListComponent, AddEditMailingTemplateComponent];