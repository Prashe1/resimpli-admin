import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditMailingTemplateComponent } from './add-edit-mailing-template.component';

describe('AddEditMailingTemplateComponent', () => {
  let component: AddEditMailingTemplateComponent;
  let fixture: ComponentFixture<AddEditMailingTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditMailingTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditMailingTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
