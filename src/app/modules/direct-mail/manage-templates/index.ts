import { ListComponent } from "./list/list.component";
import { AddEditManageTemplateComponent } from "./add-edit-manage-template/add-edit-manage-template.component";

export const components = [ListComponent, AddEditManageTemplateComponent];