import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditManageTemplateComponent } from './add-edit-manage-template.component';

describe('AddEditManageTemplateComponent', () => {
  let component: AddEditManageTemplateComponent;
  let fixture: ComponentFixture<AddEditManageTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditManageTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditManageTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
