import { ListComponent } from "./list/list.component";
import { AddEditEnvelopComponent } from "./add-edit-envelop/add-edit-envelop.component";

export const components = [ListComponent, AddEditEnvelopComponent];