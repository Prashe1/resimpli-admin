import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditEnvelopComponent } from './add-edit-envelop.component';

describe('AddEditEnvelopComponent', () => {
  let component: AddEditEnvelopComponent;
  let fixture: ComponentFixture<AddEditEnvelopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditEnvelopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditEnvelopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
