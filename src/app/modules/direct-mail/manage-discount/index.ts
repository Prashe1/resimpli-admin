import { ListComponent } from "./list/list.component";
import { AddEditDiscountComponent } from "./add-edit-discount/add-edit-discount.component";

export const components = [ListComponent, AddEditDiscountComponent];