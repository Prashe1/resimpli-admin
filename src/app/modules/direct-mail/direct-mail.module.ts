import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DirectMailRoutingModule } from './direct-mail-routing.module';
import { ListComponent } from './manage-templates/list/list.component';
import { AddEditManageTemplateComponent } from './manage-templates/add-edit-manage-template/add-edit-manage-template.component';
import { ListComponent as alias1} from './mailing-templates/list/list.component';
import { AddEditMailingTemplateComponent } from './mailing-templates/add-edit-mailing-template/add-edit-mailing-template.component';
import { AddEditEnvelopComponent } from './envelop-type/add-edit-envelop/add-edit-envelop.component';
import { AddEditPostageTypeComponent } from './postage-type/add-edit-postage-type/add-edit-postage-type.component';
import { AddEditDiscountComponent } from './manage-discount/add-edit-discount/add-edit-discount.component';
import { AddEditDcouponComponent } from './manage-coupons/add-edit-dcoupon/add-edit-dcoupon.component';
import { AddEditTargetCampaignComponent } from './manage-target-campaign/add-edit-target-campaign/add-edit-target-campaign.component';

import * as fromManageTemplates from "./manage-templates";
import * as fromMailingTemplates from "./mailing-templates";
import * as fromEnvelopType from "./envelop-type";
import * as fromPostageType from "./postage-type";
import * as fromManageDiscount from "./manage-discount";
import * as fromManageCoupons from "./manage-coupons";
import * as fromManageTargetCampaign from "./manage-target-campaign";

@NgModule({
  declarations: [
    ListComponent,
    ListComponent,
    AddEditManageTemplateComponent,
    AddEditMailingTemplateComponent,
    AddEditEnvelopComponent,
    AddEditPostageTypeComponent,
    AddEditDiscountComponent,
    AddEditDcouponComponent,
    AddEditTargetCampaignComponent,
    fromManageTemplates.components,
    fromMailingTemplates.components,
    fromEnvelopType.components,
    fromPostageType.components,
    fromManageDiscount.components,
    fromManageCoupons.components,
    fromManageTargetCampaign.components,
  ],
  imports: [
    CommonModule,
    DirectMailRoutingModule
  ]
})
export class DirectMailModule { }
