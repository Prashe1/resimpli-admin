import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  header: any = [];
  staticWidth = '3500px';
  data : any = [];
  constructor() { }

  ngOnInit(): void {
    this.data =  [
      {
        firstName :'Sharad',
        lastName :'Mehta',
        email : 'sharad@resimpli.com',
        phoneNumber: '(773) 977-9115',
        status: 'Active',
        discountCode : 'CCF',
        signUpDate: 'Jab 1, 2021',
        lastLogin: 'Feb 2, 2021',
        last30days: '20',
        dailyLoginAvg: '2',
        teamMembers : '4',
        secPhoneNumbers : '5',
        callMade : '200',
        smsSent: '300',
        RVMSent: '150',
        emailsSent :'300',
        eSignDocs : '10',
        leads: '100',
        dripCampaignsActive: '75',
        inventory:'2',
        bankAccounts: '4',
        listStacking: '50,000'
      },
        {
          firstName :'Sharad',
          lastName :'Mehta',
          email : 'sharad@resimpli.com',
          phoneNumber: '(773) 977-9115',
          status: 'Active',
          discountCode : 'CCF',
          signUpDate: 'Jab 1, 2021',
          lastLogin: 'Feb 2, 2021',
          last30days: '20',
          dailyLoginAvg: '2',
          teamMembers : '4',
          secPhoneNumbers : '5',
          callMade : '200',
          smsSent: '300',
          RVMSent: '150',
          emailsSent :'300',
          eSignDocs : '10',
          leads: '100',
          dripCampaignsActive: '75',
          inventory:'2',
          bankAccounts: '4',
          listStacking: '50,000'
        }
      ];

    this.header = [
      {
        label: 'First Name',
        key: 'firstName',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        style: { color: '#008080',position: 'sticky' },
      },
      {
        label: 'Last Name',
        key: 'lastName',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        style: { color: '#008080',position: 'sticky' },
      },
      {
        label: 'Email',
        key: 'email',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'Phone Number',
        key: 'phoneNumber',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'Status',
        key: 'status',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'Discount Code',
        key: 'discountCode',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'Signup Date',
        key: 'signUpDate',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'Last Login',
        key: 'lastLogin',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'Last 30 days login',
        key: 'last30days',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'Daily Login Avg',
        key: 'dailyLoginAvg',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'Team Members',
        key: 'teamMembers',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'Phone Numbers',
        key: 'secPhoneNumbers',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'Call Made',
        key: 'callMade',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'SMS Sent',
        key: 'smsSent',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'RVM Sent',
        key: 'RVMSent',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'Emails Sent',
        key: 'emailsSent',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'ESign Docs',
        key: 'eSignDocs',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'Leads',
        key: 'leads',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'Drip Campaigns Active',
        key: 'dripCampaignsActive',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'Inventory',
        key: 'inventory',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'Back Accounts',
        key: 'bankAccounts',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label: 'List Stacking',
        key: 'listStacking',
        // isClickExists: true,
        // event: 'EDIT_EMAIL',
        //style: { color: '#008080' },
      },
      {
        label : 'Time spent',
        key : 'timeSpent',
      },
        {
          label : 'KPIs',
          key : 'kpis',
        },
        {
          label : 'Leads',
          key : 'leads',
        },
        {
          label : 'Inventory',
          key : 'inventory',
        },
        {
          label : 'Sold',
          key : 'sold',
        },
        {
          label : 'Rental',
          key : 'rental',
        },
        {
          label : 'List Stacking',
          key : 'listStacking',
        },
        {
          label : 'Direct Mail',
          key : 'directMial',
        },
        {
          label : 'Analytics',
          key : 'analytics',
        },
        {
          label : 'Contractor',
          key : 'contractor',
        },
        {
          label : 'E-sign',
          key : 'eSign',
        },
        {
          label : 'SMS',
          key : 'sms',
        },
        {
          label : 'Task',
          key : 'task',
        },
        {
          label : 'Banking',
          key : 'banking',
        },
        {
          label : 'Vendors',
          key : 'vendors',
        },
        {
          label : 'Properties',
          key : 'properties',
        },
        {
          label : 'Account',
          key : 'account',
        },
        {
          label : 'Reports',
          key : 'reports',
        },
        {
          label : 'E-sign',
          key : 'eSign',
        },
    ];
  }
}
