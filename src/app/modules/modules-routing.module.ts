import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../providers/guard/auth.guard';
import { LazyGuard } from '../providers/guard/lazy.guard';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
    // DEFAULT
    {
      path: '',
      redirectTo: 'home',
      pathMatch: 'full',
    },
    // DASHBOARD
    {
      path: 'home',
      component: DashboardComponent,
      data: {
        title: 'home',
      },
    },
    {
      path: 'admin',
      loadChildren: () =>
        import('./admin/admin.module').then((m) => m.AdminModule)
    },
    {
      path: 'direct-mail',
      loadChildren: () =>
        import('./direct-mail/direct-mail.module').then((m) => m.DirectMailModule)
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulesRoutingModule { }
