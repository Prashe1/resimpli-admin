import { Component, OnInit } from '@angular/core';
import { SideBarListModel } from 'src/app/utils/models/sidebar';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  sideMenu: SideBarListModel[] = [];

  constructor() {}

  ngOnInit(): void {
    this.sideMenu.push(
      // CONTACTS
      {
        icon: '/assets/images/contact.svg',
        label: 'General',
        subMenu: [
          {
            icon: '/assets/images/properties.svg',
            label: 'Home',
            route: '/home',
            state: { data: { moduleId: 123 } },
          },
          {
            icon: '/assets/images/agents.svg',
            label: 'Admin',
            route: '/admin',
          },
        ],
      },
      // MARKETING
      {
        icon: '/assets/images/direct-mail.svg',
        label: 'Direct Mail',
        subMenu: [
          {
            icon: '/assets/images/direct-mail.svg',
            label: 'Manage-Template',
            route: 'direct-mail/manage-template',
          },
          {
            icon: '/assets/images/direct-mail.svg',
            label: 'Mailing-Template',
            route: 'direct-mail/mailing-template',
          },
          {
            icon: '/assets/images/direct-mail.svg',
            label: 'Envelop-Type',
            route: 'direct-mail/envelop-type',
          },
          {
            icon: '/assets/images/direct-mail.svg',
            label: 'Postage Stamps(s)',
            route: 'direct-mail/postage-type',
          },
          {
            icon: '/assets/images/direct-mail.svg',
            label: 'Discount Setup',
            route: 'direct-mail/manage-discount',
          },
          {
            icon: '/assets/images/direct-mail.svg',
            label: 'Coupon Codes',
            route: 'direct-mail/manage-coupons',
          },
          {
            icon: '/assets/images/direct-mail.svg',
            label: 'Target Campaign',
            route: 'direct-mail/target-campaign',
          }
        ],
      }
    );
  }
}
