import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { ListViewComponent } from './list-view/list-view.component';
import { TopBarComponent } from './top-bar/top-bar.component';

@NgModule({
  imports: [
    CommonModule,
    SharedRoutingModule
  ],
  exports: [
    ListViewComponent,
    TopBarComponent
  ],
  declarations: [ListViewComponent, TopBarComponent],
})
export class SharedModule { }
