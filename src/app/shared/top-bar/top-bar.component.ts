import { 
  Component,
  OnInit,
  EventEmitter,
  Input,
  Output, } from '@angular/core';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {
  @Output() callBackFunction = new EventEmitter<string>();
  sort: any[] = [
    {
      name: 'ASC',
      icon: '/assets/images/asc',
      isActive: false,
    },
    {
      name: 'DESC',
      icon: '/assets/images/desc',
      isActive: false,
    },
  ];

  view: any[] = [
    {
      name: 'LIST',
      icon: '/assets/images/list',
      isActive: false,
    },
    {
      name: 'GRID',
      icon: '/assets/images/grid',
      isActive: false,
    },
  ];
  toggle = false;

  selectedModuleId: any[] = [];
  uploadInput: EventEmitter<any>;
  // moduleData: any;
  uploadFiles: any[] = [];
  imageArr: any[] = [];
  originName: string = '';
  fileText: string = 'Select an pdf file (PDF files supported)';
  isSearchOpen: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
