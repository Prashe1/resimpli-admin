import { 
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter } from '@angular/core';


import { CommonFunctionService } from '../../utils/common-function/common-function.service'; 
@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ListViewComponent implements OnInit {

  @Input() header: any = [];
  @Input() data: any = [];
  @Input() staticWidth: any[] = [];
  @Input() moduleName: any = '';
  @Input() isInfiniteScroll: boolean = false;
  @Input() isMultiSelectEnabled: boolean = false;
  @Output() _selectedItem = new EventEmitter<any>();

  activeIconIndex;
  activeIcon;
  selectedItem: any[] = [];

  head;
  constructor(
    public utilities: CommonFunctionService,
  ) { }

  ngOnInit(): void {
    console.log(this.header);
    this.head = this.header.find((item) => {
      return item.subData;
    });
    console.log('head',this.head);
  }

  getValue(data, header) {
    try {
      if (!header || !data) {
        return;
      }

      let value = '';
      if (header.key && header.subKey) {
        value = data[header.key][header.subKey]
          ? data[header.key][header.subKey]
          : 'N/A';
      } else if (header.key) {
        value = data[header.key] ? data[header.key] : 'N/A';
      } else if (header.value) {
        value = header.value ? header.value : 'N/A';
      } else {
        value = 'N/A';
      }

      // TYPE
      if (header?.type === 'RVM' || header?.type === 'Email') {
        value = this.getValuesRVMFromId(value);
      }

      // TYPE
      if (header?.type === 'BOTH') {
        value =
          data[header?.key][header?.subKey] +
          ' ' +
          data[header?.key][header?.subKey2];
      }

      if (header?.length) {
        value = data[header.key] ? data[header.key].length : '0';
      }

      if (header?.hoverKey) {
        value = data[header?.hoverKey] || '';
      }

      return value;
    } catch (error) {
      return '';
    }
  }

  getInitials(user) {
    var initials = user.match(/\b\w/g) || [];
    return ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();
  }

  getCurrency(data, header) {
    try {
      let value = 0;
      if (header.key && header.subKey) {
        value = data[header.key][header.subKey]
          ? data[header.key][header.subKey]
          : 0;
      } else if (header.key) {
        value = data[header.key] ? data[header.key] : 0;
      } else if (header.value) {
        value = header.value ? header.value : 0;
      } else {
        value = 0;
      }

      if (header?.length) {
        value = data[header.key] ? data[header.key].length : '0';
      }

      return value;
    } catch (error) {
      return 0;
    }
  }

  getIcon(item?, type?) {
    let name = '';
    if (type?.iconType === 'FILE') {
      name = item?.fileType;
    } else if (type?.iconType === 'CALL_LOGS') {
      name = item?.type;
    } else if (item?.iconType === 'TASKS') {
      name = item?.type;
    }

    if (!name) {
      return 'other_icon.png';
    }

    if (
      name === 'doc' ||
      name === 'docx' ||
      name === 'xls' ||
      name === 'xlsx'
    ) {
      return 'doc_icon.png';
    }

    if (name === 'mp4') {
      return 'mp4_icon.png';
    }

    if (name === 'jpeg' || name === 'jpg' || name === 'png') {
      return 'jpeg_icon.png';
    }

    // OUTGOING
    if (name === 'outgoingCall') {
      return 'outgoing-call.png';
    }

    if (name === 'outgoingSms') {
      return 'outgoing-sms.png';
    }

    if (name === 'outgoingMms') {
      return 'outgoing-mms.png';
    }

    // INCOMING
    if (name === 'incomingCall') {
      return 'incoming-call.png';
    }

    if (name === 'incomingSms') {
      return 'incoming-sms.png';
    }

    if (name === 'incomingMms') {
      return 'incoming-mms.png';
    }

    // TASKS
    if (name === 'reminder') {
      return 'notification-bell.png';
    }

    if (name === 'description') {
      return 'description-icon.png';
    }

    return name;
  }

  getValuesRVMFromId(value :string){
    return value = 'hello';
  }

  getActionIcon(action, actionIndex) {
    return this.activeIconIndex === actionIndex &&
      this.activeIcon === action?.type
      ? action?.activeIcon
      : action?.icon;
  }

  selectItem(item, $event) {
    $event.stopPropagation();
    const index = this.selectedItem.findIndex((x) => x?._id === item?._id);
    if (index > -1) {
      this.selectedItem.splice(index, 1);
    } else {
      this.selectedItem.push(item);
    }

    this._selectedItem.emit(this.selectedItem);
  }

  callAction(type, item, index, moduleId?) {}

}
