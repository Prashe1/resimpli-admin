import { Injectable } from '@angular/core';
import * as moment from 'moment-timezone';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommonFunctionService {
  constructor() {}

  orderItems(data, key) {
    const sortedData = data.sort((a, b) => a[key] - b[key]);

    return sortedData;
  }

  swap(data, fromIndex, toIndex) {
    return data.map((current, currentIndex) => {
      if (currentIndex === fromIndex) return data[toIndex];
      if (currentIndex === toIndex) return data[fromIndex];

      return current;
    });
  }

  reOrder(data, fromIndex, toIndex) {
    if (fromIndex === toIndex) return data;

    const newArray = [...data];

    const target = newArray[fromIndex];
    const inc = toIndex < fromIndex ? -1 : 1;

    for (let i = fromIndex; i !== toIndex; i += inc) {
      newArray[i] = newArray[i + inc];
    }

    newArray[toIndex] = target;

    return newArray;
  }

  maskNumber(tel) {
    try {
      if (!tel) {
        return '';
      }
      var value = tel.toString().trim().replace(/^\+/, '');
      if (value.match(/[^0-9]/)) {
        return tel;
      }
      var country, city, number;
      switch (value.length) {
        case 10:
          country = 1;
          city = value.slice(0, 3);
          number = value.slice(3);
          break;
        case 11:
          country = value.slice(0, 1);
          city = value.slice(1, 4);
          number = value.slice(4);
          break;
        case 12:
          country = value.slice(0, 2);
          city = value.slice(2, 5);
          number = value.slice(5);
          break;
        default:
          return tel;
      }

      number = number.slice(0, 3) + '-' + number.slice(3);
      // return ('+' + country + ' (' + city + ') ' + number).trim();
      return ('(' + city + ') ' + number).trim();
    } catch (error) {
      return tel;
    }
  }

  unMaskNumber(tel) {
    try {
      return tel
        .replace('(', '')
        .replace(')', '')
        .replace(' ', '')
        .replace('-', '');
    } catch (error) {
      return tel;
    }
  }

  getCountryCode(tel) {
    try {
      switch (tel?.length) {
        case 12:
          return tel.slice(0, 2);

        case 13:
          return tel.slice(0, 3);
      }
    } catch (error) {
      return '+91';
    }
  }

  capitalizeName(name) {
    let words = name.split(' ');

    for (let i = 0; i < words.length; i++) {
      words[i] = words[i][0].toUpperCase() + words[i].substr(1).toLowerCase();
    }

    return words.join(' ');
  }

  camelCaseToWord(text, style?) {
    if (!text) {
      return;
    }

    let result = text.replace(/([A-Z])/g, ' $1');
    if (style && style?.index) {
      if (style?.trasnform === 'uppercase') {
        var split = result.split(' ');
        split[style?.index] = split[style?.index].toUpperCase();
        result = split.join(' ');
      }
    }

    return result.charAt(0).toUpperCase() + result.slice(1);
  }

  minutesList = (num, fill) => {
    return new Array(num).fill(fill).map((x, i) => i + 1);
  };

  titleCaseToWord(text) {
    return text.toLowerCase().replace(/\b(\w)/g, (s) => s.toUpperCase());
  }

  taskTimeTypeList = () => {
    return ['Minutes', 'Hours', 'Weeks', 'Days', 'Months'];
  };

  isAfterBeforeDropDown = (title) => {
    if (
      title == 'no contact made' ||
      title == 'contact made' ||
      title == 'offers made' ||
      title == 'warm lead' ||
      title == 'new inventory' ||
      title == 'listed for sale' ||
      title == 'sell side - under contract' ||
      title == 'rental application approved' ||
      title == 'listed for rent' ||
      title == 'sold' ||
      title == 'rental'
    ) {
      return false;
    } else {
      return true;
    }
  };

  sortByObject = (moduleList) =>
    moduleList.reduce((obj, item, index) => {
      return {
        ...obj,
        [item]: index,
      };
    }, {});

  timezoneLists = () => {
    return [
      {
        name: 'United States (Honolulu)',
        catvalue:
          moment.tz('Pacific/Honolulu').format('Z') + '~Pacific/Honolulu',
        value: moment.tz('Pacific/Honolulu').format('Z'),
        timezonename: 'Pacific/Honolulu',
        index: 0,
      },
      {
        name: 'United States (Anchorage)',
        catvalue:
          moment.tz('America/Anchorage').format('Z') + '~America/Anchorage',
        value: moment.tz('America/Anchorage').format('Z'),
        timezonename: 'America/Anchorage',
        index: 1,
      },
      {
        name: 'United States (Los_Angeles)',
        catvalue:
          moment.tz('America/Los_Angeles').format('Z') + '~America/Los_Angeles',
        value: moment.tz('America/Los_Angeles').format('Z'),
        timezonename: 'America/Los_Angeles',
        index: 2,
      },
      {
        name: 'United States (Denver)',
        catvalue: moment.tz('America/Denver').format('Z') + '~America/Denver',
        value: moment.tz('America/Denver').format('Z'),
        timezonename: 'America/Denver',
        index: 3,
      },
      {
        name: 'United States (Chicago)',
        catvalue: moment.tz('America/Chicago').format('Z') + '~America/Chicago',
        value: moment.tz('America/Chicago').format('Z'),
        timezonename: 'America/Chicago',
        index: 4,
      },
      {
        name: 'United States (New_York)',
        catvalue:
          moment.tz('America/New_York').format('Z') + '~America/New_York',
        value: moment.tz('America/New_York').format('Z'),
        timezonename: 'America/New_York',
        index: 5,
      },
    ];
  };

  dueDateFormat = (dueDate) => {
    return {
      date: moment(dueDate).format('MMM Do YYYY'),
      time: moment(dueDate).format('LT'),
    };
  };

  uniqueValues = (data, key?, key1?) => {
    const unique = Array.from(
      new Set(
        data.map((item) => {
          return key1 ? item[key][key1] : item[key];
        })
      )
    ).map((title) => {
      return data.find((t) => {
        return key1 ? t[key][key1] === title : t[key] === title;
      });
    });

    const uniqueValues = unique.map((item) => {
      return {
        label: key1 ? item[key][key1] : item[key],
        value: key1 ? item[key][key1] : item[key],
      };
    });

    return uniqueValues;
  };

  getSum = (data, key) => {
    return data.reduce((a, b) => a + (b[key] || 0), 0);
  }
}
