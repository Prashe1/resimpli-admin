export interface SideBarListModel {
    icon: any;
    label: string;
    subMenu: SubMenu[];
  }
  
  export interface SubMenu {
    icon: string;
    label: string;
    route: string;
    queryParams?: any;
    state?: any;
    target?: string;
  }
  